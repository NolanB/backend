from django.apps import AppConfig


class WattSimAppConfig(AppConfig):
    name = 'wattsim.app'

    def ready(self):
        import wattsim.app.signals  # noqa: F401
