from django.db import models
from django.conf import settings
import uuid

from django.contrib.auth.models import AbstractUser


class Station(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    # user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)


class Measurement(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    recorded = models.DateTimeField(auto_now_add=True)
    solar = models.FloatField(blank=True, null=True)
    wind = models.FloatField(blank=True, null=True)
    hydro = models.FloatField(blank=True, null=True)
    station = models.ForeignKey(Station, on_delete=models.CASCADE)

    class Meta:
        default_related_name = 'measurements'
        get_latest_by = 'recorded'
        ordering = ('recorded', )




class User(AbstractUser):
    is_student = models.BooleanField(default=False)
    is_teacher = models.BooleanField(default=False)

class Student(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    quizzes = models.ManyToManyField(Quiz, through='TakenQuiz')
    interests = models.ManyToManyField(Subject, related_name='interested_students')
