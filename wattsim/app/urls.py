from django.urls import path
from wattsim.app.views import UserAPIView, MeasurementAPIView, LatestMeasurementAPIView


app_name = 'wattsim.app'
urlpatterns = [
    path('user/', UserAPIView.as_view(), name='user'),
    path('measurements/latest/', LatestMeasurementAPIView.as_view(), name='latest-measurement'),
    path('measurements/', MeasurementAPIView.as_view(), name='measurements'),
]


from django.urls import include
from wattsim.app.views import StudentSignUpView, TeacherSignUpView, SignUpView

urlpatterns = [
    path('', include('classroom.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('accounts/signup/', SignUpView.as_view(), name='signup'),
    path('accounts/signup/student/', StudentSignUpView.as_view(), name='student_signup'),
    path('accounts/signup/teacher/', TeacherSignUpView.as_view(), name='teacher_signup'),
]