from django.contrib.auth import logout
from rest_framework.exceptions import NotFound
from rest_framework.generics import RetrieveAPIView, RetrieveDestroyAPIView, ListCreateAPIView
from rest_framework.mixins import CreateModelMixin
from rest_framework.permissions import IsAuthenticated
from wattsim.app.models import Measurement
from wattsim.app.serializers import UserSerializer, MeasurementSerializer


class UserAPIView(CreateModelMixin, RetrieveDestroyAPIView):
    """
    Login, view the current user or logout.
    """
    name = 'Authentication'
    serializer_class = UserSerializer

    def get_object(self):
        return self.request.user

    def post(self, *args, **kwargs):
        return self.create(*args, **kwargs)

    def perform_destroy(self, instance):
        logout(self.request)


class MeasurementAPIView(ListCreateAPIView):
    """
    Browse the user's station's measurements or save a new measurement.
    """
    name = 'Measurements'
    serializer_class = MeasurementSerializer
    permission_classes = (IsAuthenticated, )

    def get_queryset(self):
        return Measurement.objects.filter(station__user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(station=self.request.user.station)


class LatestMeasurementAPIView(RetrieveAPIView):
    name = 'Latest measurement'
    serializer_class = MeasurementSerializer
    permission_classes = (IsAuthenticated, )

    def get_object(self):
        try:
            return Measurement.objects \
                .filter(station__user=self.request.user) \
                .latest()
        except Measurement.DoesNotExist:
            raise NotFound


from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from decorators import student_required
from django.shortcuts import redirect, render
from django.shortcuts import redirect, render
from django.views.generic import CreateView
from django.views.generic import TemplateView
from django.utils.decorators import method_decorator

from wattsim.app.forms import StudentSignUpForm
from wattsim.app.models import User


class StudentSignUpView(CreateView):
    model = User
    form_class = StudentSignUpForm
    template_name = 'registration/signup_form.html'

    def get_context_data(self, **kwargs):
        kwargs['user_type'] = 'student'
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('students:quiz_list')





@method_decorator([login_required, student_required], name='dispatch')
class StudentInterestsView(UpdateView):
    model = Student
    form_class = StudentInterestsForm
    template_name = 'classroom/students/interests_form.html'
    success_url = reverse_lazy('students:quiz_list')

    def get_object(self):
        return self.request.user.student

    def form_valid(self, form):
        messages.success(self.request, 'Interests updated with success!')
        return super().form_valid(form)


class TeacherSignUpView(CreateView):
    model = User
    form_class = TeacherSignUpForm
    template_name = 'registration/signup_form.html'

    def get_context_data(self, **kwargs):
        kwargs['user_type'] = 'teacher'
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('teachers:quiz_change_list')





class SignUpView(TemplateView):
    template_name = 'registration/signup.html'


def home(request):
    if request.user.is_authenticated:
        if request.user.is_teacher:
            return redirect('teachers:quiz_change_list')
        else:
            return redirect('students:quiz_list')
    return render(request, 'classroom/home.html')