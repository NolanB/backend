from django.contrib.auth import get_user_model
from django.db.models.signals import post_save
from django.dispatch import receiver


@receiver(post_save, sender=get_user_model())
def autocreate_user_related_models(sender, instance=None, created=False, **kwargs):
    if created:
        from rest_framework.authtoken.models import Token
        from wattsim.app.models import Station
        Token.objects.create(user=instance)
        Station.objects.create(user=instance)
